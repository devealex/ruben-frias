<?php include('components/header.php'); ?>

<section class="is-page is-about-section">
    <div class="is-header-img">
        <h1>Bufete Jurídico Frías</h1>
    </div>

    <div class="container">
        <div class="columns">
            <div class="column is-full is-overview">
                <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">¿Quiénes Somos?</h2>

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".9s">Somos una firma Jurídica que tiene como misión practicar la excelencia, lo hacemos desde hace mas de veinte años, dentro de nuestras oficinas en Morelos, Quintana Roo y Ciudad de México. Contamos con un equipo de profesionales, especialistas en diferentes ramas del derecho al servicio de la comunidad empresarial en México y de una diversidad de clientes e inversionistas de los principales centros de negocios del País.  Asimismo, con el fin de atender y acompañar a todos nuestros clientes dentro de un ámbito de honestidad, guardamos una estrecha relación con una red formada por las mejores firmas legales en toda la República Mexicana.</p>

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s"> La solidez de nuestra estructura Institucional nos permite tener un espíritu de constante cambio y movimiento. Sabemos que los más altos estándares exigen un estado de permanente renovación. Conocemos nuestra profesión mejor que nadie, creemos en la actualización profesional y académica, por lo que todos nuestros Abogados cuentan con estudios de posgrado en diferentes ramas del Derecho.</p>

            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="columns">
            <div class="column is-full is-markers">
                <img src="<?=_IMG.'mexico-map.png';?>">

                <img src="<?=_IMG.'marker-location.png';?>" class="is-marker is-cancun">
                <span class="is-title-cancun"></span>
                <img src="<?=_IMG.'marker-location.png';?>" class="is-marker is-mexico">
                <span class="is-title-mexico"></span>
                <img src="<?=_IMG.'marker-location.png';?>" class="is-marker is-puebla">
                <span class="is-title-puebla"></span>
            </div>
        </div>
    </div>

    <div class="container">
    <div class="columns is-multiline is-locations">
        <div class="column is-full"><h2 class="wow slideInRight" data-wow-duration="1s" data-wow-delay=".5s">Estamos en...</h2></div>

        <div class="column is-one-third is-item wow slideInRight" data-wow-duration="1s" data-wow-delay=".7s">
            <img src="<?= _IMG . 'cancun-bufete-frias.jpg'; ?>">
            <strong>Cancún / Playa del Carmen</strong>
            <span><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
        </div>

        <div class="column is-one-third is-item wow slideInRight" data-wow-duration="1s" data-wow-delay=".6s">
            <img src="<?= _IMG . 'mexico-bufete-frias.jpg'; ?>">
            <strong>Ciudad de México</strong>
            <span><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
        </div>

        <div class="column is-one-third is-item wow slideInRight" data-wow-duration="1s" data-wow-delay=".8s">
            <img src="<?= _IMG . 'puebla-bufete-frias.jpg'; ?>">
            <strong>Morelos</strong>
            <span><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
        </div>

        </div>
    </div>

</section>

<?php include('components/footer.php'); ?>