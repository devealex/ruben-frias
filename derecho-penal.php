<?php include('components/header.php'); ?>

<section class="is-page is-service-page-section">
    <div class="is-header-img">
        <h1>Derecho Penal</h1>
    </div>

    <div class="container">
        <div class="columns is-overview">
           
            <div class="column is-half">

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">Como Abogados Penalistas, somos un equipo altamente calificado y  especializado en Derecho Penal, brindando atención oportuna y manejo de conflicto en tiempo real ante cualquier tribunal. Tomando en consideración que, los primeros momentos en los conflictos penales son los que determinan en la mayoría de los casos, los resultados pretendidos en beneficio de nuestro cliente. Nuestra firma ofrece distintos servicios en materia Penal, entre los que destacan:</p>

            </div>

            <div class="column is-half">

                <strong>¿Alguno de estos es tu caso y no sabes qué hacer?</strong>

                <ul class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                    <li><i class="fas fa-gavel"></i>Denuncias</li>
                    <li><i class="fas fa-gavel"></i>Representación ante el mp</li>
                    <li><i class="fas fa-gavel"></i>Defensa penal</li>
                    <li><i class="fas fa-gavel"></i>Asesoría jurídica a victima</li>
                    <li><i class="fas fa-gavel"></i>Prevención a delitos</li>
                    <li><i class="fas fa-gavel"></i>Representación en juicio</li>
                </ul>

            </div>

        </div>
    </div>

    <?php include('components/service-contact-section.php'); ?>

</section>

<?php include('components/footer.php'); ?>