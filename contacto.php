<?php include('components/header.php'); ?>

<section class="is-page is-contact-section">
    <div class="is-header-img">
        <h1>Bufete Jurídico Frías</h1>
    </div>

    <div class="container">
        <div class="columns is-locations">

            <div class="column is-one-third wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
                <h3>Cancún / Playa del Carmen</h3>

                <!--<span><i class="fas fa-map-marker-alt"></i> SM 521, Mza 5 L96B. Villas Cancún. CP. 77536</span>-->
                <span><i class="far fa-envelope"></i> asesoria@bufetejuridicofrias.com</span>
                <span><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>
                <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
                
            </div>

            <div class="column is-one-third wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                <h3>Ciudad de México</h3>

                <!--<span><i class="fas fa-map-marker-alt"></i> SM 521, Mza 5 L96B. Villas Cancún. CP. 77536</span>-->
                <span><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
                <span><i class="fas fa-mobile-alt"></i> (554) 415 5117</span>
                <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
                
            </div>

            <div class="column is-one-third wow fadeInUp" data-wow-duration="1s" data-wow-delay=".9s">
                <h3>Morelos</h3>

                <!--<span><i class="fas fa-map-marker-alt"></i> SM 521, Mza 5 L96B. Villas Cancún. CP. 77536</span>-->
                <span><i class="far fa-envelope"></i> asesoria@bufetejuridicofrias.com</span>
                <span><i class="fas fa-mobile-alt"></i> (735) 323 2660</span>
                <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
                
            </div>

        </div>
    </div>


    <div class="is-form">
        <div class="columns">

            <div class="column is-half">
                <div class="is-text">
                    <h2 class="wow slideInLeft" data-wow-duration="1s" data-wow-delay=".5s">Ecríbenos</h2>

                    <span class="wow slideInLeft" data-wow-duration="1s" data-wow-delay=".8s"><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
                    <span class="wow slideInLeft" data-wow-duration="1s" data-wow-delay=".9s"><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
                    <span class="wow slideInLeft" data-wow-duration="1s" data-wow-delay="1.1s"><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>

                    <a href="https://www.facebook.com/bufetejuridico.frias" target="_blank" class="is-social is-facebook wow slideInLeft" data-wow-duration="1s" data-wow-delay="1.1s"><i class="fab fa-facebook-f"></i> En Facebook</a>
                    <a href="https://www.instagram.com/juridicofrias/" target="_blank" class="is-social is-instagram wow slideInLeft" data-wow-duration="1s" data-wow-delay="1.3s"><i class="fab fa-instagram"></i> En Instagram</a>

                </div>
            </div>

            <div class="column is-half is-frm wow slideInRight" data-wow-duration="1s" data-wow-delay="1s">
                    <form name="frmServiceDetail" id="frmServiceDetail" action="/hola" method="post" class="columns is-multiline">
                        <fieldset class="column is-full">
                            <label>Nombre completo</label>
                            <input type="text" name="inpName" id="inpName">
                        </fieldset>
                        <fieldset class="column is-two-thirds">
                            <label>Email</label>
                            <input type="email" name="inpEmail" id="inpEmail">
                        </fieldset>
                        <fieldset class="column is-one-third">
                            <label>Teléfono</label>
                            <input type="number" name="inpNumberPhone" id="inpNumberPhone">
                        </fieldset>
                        <fieldset class="column is-half">
                            <label>Interesado en</label>
                            <select name="inpServiceInteresting" id="inpServiceInteresting">
                                <option value="Laboral">Derecho Laboral</option>
                                <option value="Penal">Derecho Penal</option>
                                <option value="Mercantil">Derecho Mercantil</option>
                                <option value="Civil">Derecho Civil</option>
                                <option value="Familiar">Derecho Familiar</option>
                                <option value="Notarial">Derecho Notarial</option>
                                <option value="Migratorio">Derecho Migratorio</option>
                                <option value="Empresarial">Derecho Empresarial</option>
                                <option value="Amparo">Derecho de Amparo</option>
                            </select>
                        </fieldset>
                        <fieldset class="column is-half">
                            <label>¿Dónde estas?</label>
                            <select name="inpWhere" id="inpWhere">
                                <option value="Cancun/Playa">Cancún / Playa del Carmen</option>
                                <option value="CMexico">Ciudad de México</option>
                                <option value="Morelos">Morelos</option>
                            </select>
                        </fieldset>
                        <fieldset class="column is-full">
                            <label>Comentarios</label>
                            <textarea name="inpComments" id="inpComments" rows="5"></textarea>
                        </fieldset>
                        <fieldset class="column is-full">
                            <a id="submit-contact" href="#"><i class="fas fa-fax"></i> Contactar</a>
                            <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response">
                        </fieldset>
                    </form>
            </div>

        </div>
    </div>

</section>

<?php include('components/footer.php'); ?>