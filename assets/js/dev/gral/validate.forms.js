$(document).ready(function(){
    $('a#submit-contact').on('click', function(e){
        e.preventDefault();
        $('#frmServiceDetail').submit();
    });
    $('#frmServiceDetail').on('submit', function(){
        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        if ($('#inpName').val().trim() == '') {
            $('#inpName').focus();
            return false;
        } else if ($('#inpEmail').val().trim() == '' || regex.test($('#inpEmail').val().trim()) === false) {
            $('#inpEmail').focus();
            return false;
        } else if ($('#inpNumberPhone').val().trim() == '' || $('#inpNumberPhone').val().trim().length < 10) {
            $('#inpNumberPhone').focus();
            return false;
        } else if ($('#inpSubject').length == 0 && $('#inpServiceInteresting').val().trim() == 'Any' && $('#inpComments').val().trim() == '') {
            $('#inpComments').focus();
            return false;
        } else if ($('#inpSubject').length > 0 && $('#inpSubject').val().trim() == '') {
            $('#inpSubject').focus();
            return false;
        } else {
            return true;
        }
    });

    //Home
    $('form#inpLightForm').on('submit', function(e){
        //e.preventDefault();
        var email = $('#inpEmail').val().trim();
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if ($('#inpName').val().trim() == '') {
            $('#inpName').focus();
            return false;
        } else if (email == '' || emailReg.test(email) === false) {
            $('#inpEmail').focus();
            return false;
        } else if ($('#inpNumber').val().trim() == '' || $('#inpNumber').val().trim().length < 10) {
            $('#inpNumber').focus();
            return false;
        } else {
            alert('Gracias por contactar con nosotros!');
            return true;
        }
    });
});