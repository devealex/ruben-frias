<?php include('components/header.php'); ?>

<section class="is-page is-service-page-section">
    <div class="is-header-img">
        <h1>Derecho Mercantil</h1>
    </div>

    <div class="container">
        <div class="columns is-overview">
           
            <div class="column is-half">

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">Cualquier acto de comercio, está sujeto al Derecho Mercantil, por lo que ante posibles dificultades que se nos presenten al realizar nuestras actividades comerciales, es recomendable recibir la asesoría legal de un abogado especialista en derecho mercantil, para poder proteger nuestros intereses y hacer valer nuestros derechos. Nuestra firma ofrece distintos servicios en materia Mercantil, entre los que destacan:</p>

            </div>

            <div class="column is-half">

                <strong>¿Alguno de estos es tu caso y no sabes qué hacer?</strong>

                <ul class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                    <li><i class="fas fa-gavel"></i>Recuperación extrajudicial</li>
                    <li><i class="fas fa-gavel"></i>Pagarés</li>
                    <li><i class="fas fa-gavel"></i>Representación ante condusef</li>
                    <li><i class="fas fa-gavel"></i>Cheques devueltos y robados</li>
                    <li><i class="fas fa-gavel"></i>Títulos de crédito</li>
                    <li><i class="fas fa-gavel"></i>Cobros indebidos en tarjetas (debito y crédito)</li>
                </ul>

            </div>

        </div>
    </div>

    <?php include('components/service-contact-section.php'); ?>

</section>

<?php include('components/footer.php'); ?>