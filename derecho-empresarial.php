<?php include('components/header.php'); ?>

<section class="is-page is-service-page-section">
    <div class="is-header-img">
        <h1>Derecho Empresarial</h1>
    </div>

    <div class="container">
        <div class="columns is-overview">
           
            <div class="column is-half">

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">Asesoramos constantemente a empresas desde su constitución mediante la creación de estrategias legales, preparación, negociación de documentación constitutiva y corporativa, hasta la elaboración y conservación de libros y documentos corporativos. También ofrecemos servicio legal con respecto a la adquisición, enajenación, consolidación, fusión o escisión de toda clase de personas morales y organizaciones, auditorias legales (Due Diligence), fideicomisos y en la adquisición, enajenación, arrendamiento y constitución de garantías sobre toda clase de bienes muebles e inmuebles. Nuestra firma ofrece distintos servicios en Derecho Empresarial, entre los que destacan:</p>

            </div>

            <div class="column is-half">

                <strong>¿Alguno de estos es tu caso y no sabes qué hacer?</strong>

                <ul class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                    <li><i class="fas fa-gavel"></i>Startups</li>
                    <li><i class="fas fa-gavel"></i>Auditoria legal</li>
                    <li><i class="fas fa-gavel"></i>Cumplimiento normativo</li>
                    <li><i class="fas fa-gavel"></i>Administración jurídica</li>
                    <li><i class="fas fa-gavel"></i>Mediación</li>
                    <li><i class="fas fa-gavel"></i>Prevención de riesgos</li>
                    <li><i class="fas fa-gavel"></i>Cierre de un negocio</li>
                </ul>

            </div>

        </div>
    </div>

    <?php include('components/service-contact-section.php'); ?>

</section>

<?php include('components/footer.php'); ?>