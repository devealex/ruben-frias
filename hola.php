<?php include('components/header.php'); ?>
<?php
if (isset($_POST['inpName']) && trim($_POST['inpName']) != '') {
    //Validar Captcha
    $secret_key = '6LcuiegUAAAAAIVzWH9GJa-OnsTS1ah4XN8kEBqr';
    $captcha = $_POST['g-recaptcha-response'];
    $rs = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$captcha);
    $rt = json_decode($rs, TRUE);

    //Envio del correo
    if ($rt['success'] == true) {
        $headers = 'MIME-Version:1.0'."\r\n".
				'Content-type:text/html; charset=UTF-8'."\r\n".
                'From: hi@bufetejuridicofrias.com'. "\r\n" .
                'Cc: rfrias@bufetejuridicofrias.com';
                //'Bcc: by@creativoapp.com'. "\r\n";
        
        date_default_timezone_set('America/Cancun');
        
        $message = '<div style="background: #F8F8F8; padding: 30px 0;">
                    <table style="width: 700px; border: 0; border-collapse: collapse; background: #fff; margin: 30px auto; border-radius: 5px;" border="0" cellpadding="20">
                        <tr>
                            <td colspan="2">
                                <a href="http://www.bufetejuridicofrias.com"><img style="width:100px; display:block; margin:0px auto 30px;" src="https://www.bufetejuridicofrias.com/assets/img/bufete-juridico-frias.png"></a>
                                <strong style="display: block; color: #73501f; font-size: 20px; text-align: center;">CONTACTO</strong>
                                <span style="display: block; font-size: 14px; text-align: center; color: #666; margin-top: 15px;">Interesado en:</span>
                                <small style="display: block; color: #73501f; font-size: 16px; text-align: center;">'.$_POST["inpServiceInteresting"].'</small>
                            </td>
                        </tr>

                        <tr><td colspan="2"><strong style="font-size: 16px; color: #000;">Detalles del contacto:</strong></td></tr>

                        <tr>
                            <td><span>Nombre:</span></td>
                            <td><strong>'.$_POST['inpName'].'</strong></td>
                        </tr>

                        <tr>
                            <td><span>Email:</span></td>
                            <td><strong>'.$_POST['inpEmail'].'</strong></td>
                        </tr>

                        <tr>
                            <td><span>Telefono:</span></td>
                            <td><strong>'.$_POST['inpNumberPhone'].'</strong></td>
                        </tr>

                        <tr>
                            <td><span>Destino:</span></td>
                            <td><strong>'.$_POST['inpWhere'].'</strong></td>
                        </tr>

                        <tr>
                            <td><span>Tipo de servicio:</span></td>
                            <td><strong>'.$_POST["inpServiceInteresting"].'</strong></td>
                        </tr>

                        <tr><td colspan="2"><span>Comentarios:</span></td></tr>
                        <tr><td colspan="2"><strong>'.$_POST['inpComments'].'</strong></td></tr>

                    </table>

                    <p style="text-align:center;">Formulario enviado desde: '.$_SERVER["HTTP_REFERER"].'</p>
                    <p style="text-align:center;">Formulario enviado: '.date('Y-m-d H:i:s').'</p>
                </div>';
        
        mail('asesoria@bufetejuridicofrias.com', 'Contacto | Bufete Jurídico Frías', $message, $headers);
        ?>
            <section class="is-page is-contact-section">
                <div class="is-header-img">
                    <h1>Bufete Jurídico Frías</h1>
                </div>
            </section>
            <div class="container wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                <h1 style="text-align: center;">Excelente!<br>Gracias por contactarte con nosotros<small>(Nos comunicaremos con tigo)</small></h1>
                <br><br><br><br>
            </div>
        <?php
    } else {
        ?>
            <section class="is-page is-contact-section">
                <div class="is-header-img">
                    <h1>Bufete Jurídico Frías</h1>
                </div>
            </section>
            <div class="container wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                <h1 style="text-align: center;">Ouch!<br>Hubo un problema con el envio <small>(Intentelo mas tarde)</small></h1>
                <br><br><br><br>
            </div>
        <?php
    }
} else {
    ?>
    <section class="is-page is-contact-section">
        <div class="is-header-img">
            <h1>Bufete Jurídico Frías</h1>
        </div>
    </section>
    <br><br>
    <?php
}
?>
<?php include('components/footer.php'); ?>