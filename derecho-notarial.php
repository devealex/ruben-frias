<?php include('components/header.php'); ?>

<section class="is-page is-service-page-section">
    <div class="is-header-img">
        <h1>Derecho Notarial</h1>
    </div>

    <div class="container">
        <div class="columns is-overview">
           
            <div class="column is-half">

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">Una de las fortalezas dentro de nuestros servicios son todas las normas jurídicas y principios regístrales que regulan la organización y funcionamiento de los registros públicos, los derechos inscribibles y medidas precautorias en los diversos registros, en relación con terceros. Nuestra firma ofrece distintos servicios en materia Notarial y Registral, entre los que destacan:</p>

            </div>

            <div class="column is-half">

                <strong>¿Alguno de estos es tu caso y no sabes qué hacer?</strong>

                <ul class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                    <li><i class="fas fa-gavel"></i>Escrituracion</li>
                    <li><i class="fas fa-gavel"></i>Testamento</li>
                    <li><i class="fas fa-gavel"></i>Fe de hechos</li>
                    <li><i class="fas fa-gavel"></i>Copias certificadas</li>
                    <li><i class="fas fa-gavel"></i>Poderes notariales</li>
                    <li><i class="fas fa-gavel"></i>Protocolización de actas</li>
                    <li><i class="fas fa-gavel"></i>Constitución de sociedades</li>
                </ul>

            </div>

        </div>
    </div>

    <?php include('components/service-contact-section.php'); ?>

</section>

<?php include('components/footer.php'); ?>