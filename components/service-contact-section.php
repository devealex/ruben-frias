<div class="is-contact">
        <div class="columns">

            <div class="column is-half is-locations">
                <div class="is-box">
                    <h3 class="wow slideInLeft" data-wow-duration="1s" data-wow-delay=".5s">¿Dónde estamos?</h3>

                    <p class="wow slideInLeft" data-wow-duration="1s" data-wow-delay=".5s">Tenemos operación y asistencia jurídica en el Estado de Morelos, Quintana Roo y Ciudad de México. Los 365 días del año, las 24 horas.</p>

                    <div class="wow slideInLeft" data-wow-duration="1s" data-wow-delay=".9s"> 
                        <strong>Cancún / Playa del Carmen</strong> 
                        <!--<span><i class="fas fa-map-marker-alt"></i> SM 521, Mza 5 L96B. Villas Cancún. CP. 77536</span>-->
                        <span><i class="far fa-envelope"></i> asesoria@bufetejuridicofrias.com</span>
                        <span><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>
                        <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
                    </div>

                    <div class="wow slideInLeft" data-wow-duration="1s" data-wow-delay=".7s">
                        <strong>Ciudad de México</strong>
                        <!--<span><i class="fas fa-map-marker-alt"></i> SM 521, Mza 5 L96B. Villas Cancún. CP. 77536</span>-->
                        <span><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
                        <span><i class="fas fa-mobile-alt"></i> (554) 415 5117</span>
                        <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
                    </div>

                    <div class="wow slideInLeft" data-wow-duration="1s" data-wow-delay="1.1s">
                        <strong>Morelos</strong>  
                        <!--<span><i class="fas fa-map-marker-alt"></i> SM 521, Mza 5 L96B. Villas Cancún. CP. 77536</span>-->
                        <span><i class="far fa-envelope"></i> asesoria@bufetejuridicofrias.com</span>
                        <span><i class="fas fa-mobile-alt"></i> (735) 323 2660</span>
                        <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
                    </div>

                </div>
            </div>
        
            <div class="column is-half is-form">
                <div class="is-box">
                    <h3 class="wow slideInRight" data-wow-duration="1s" data-wow-delay=".5s">Contáctanos</h3>

                    <!--<p class="wow slideInRight" data-wow-duration="1s" data-wow-delay=".5s">Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>-->

                    <?php 
                    $serviceInteresting = 'Service Page';
                    include('service-form.php'); ?>
                </div>
            </div>

        </div>
    </div>