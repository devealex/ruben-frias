
    <footer class="is-footer">
        <div class="container">
            <div class="columns">

                <div class="column is-one-third is-brand">
                    <a href="/"><img src="/assets/img/bufete-juridico-frias.png"></a>
                    <div class="is-social">
                        <a href="https://www.facebook.com/bufetejuridico.frias" target="_blank" class="is-facebook"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://www.instagram.com/juridicofrias/" target="_blank" class="is-instagram"><i class="fab fa-instagram"></i></a>
                    </div>

                    <small class="is-by">© <?=date('Y');?> Bufete Jurídico Frías. Todos los derechos reservados.</small>
                </div>

                <div class="column">
                    <strong>Generales</strong>
                    <ul>
                        <li><a href="/">Inicio</a></li>
                        <li><a href="/bufete-frias">Nosotros</a></li>
                        <li><a href="/servicios">Servicios</a></li>
                        <li><a href="/blog/">Blog</a></li>
                    </ul>
                </div>

                <div class="column">
                    <strong>Servicios</strong>
                    <ul>
                        <li><a href="/derecho-laboral">Derecho Laboral</a></li>
                        <li><a href="/derecho-penal">Derecho Penal</a></li>
                        <li><a href="/derecho-mercantil">Derecho Mercantil</a></li>
                        <li><a href="/derecho-civil">Derecho Civil</a></li>
                        <li><a href="/derecho-familiar">Derecho Familiar</a></li>
                        <li><a href="/derecho-notarial">Derecho Notarial</a></li>
                        <li><a href="/derecho-migratorio">Derecho Migratorio</a></li>
                        <li><a href="/derecho-empresarial">Derecho Empresarial</a></li>
                        <li><a href="/derecho-de-amparo">Derecho de Amparo</a></li>
                    </ul>
                </div>

                <div class="column">
                    <strong>Contacto</strong>
                    
                    <div class="is-item">
                        <span>Cancún / Playa del Carmen</span>
                        <small><i class="far fa-envelope"></i> asesoria@bufetejuridicofrias.com</small>
                        <small><i class="fas fa-mobile-alt"></i> (998) 440 2829</small>
                        <small><i class="fas fa-mobile-alt"></i> (998) 140 3087</small>
                    </div>

                    <div class="is-item">
                        <span>Ciudad de México</span>
                        <small><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</small>
                        <small><i class="fas fa-mobile-alt"></i> (554) 415 5117</small>
                        <small><i class="fas fa-mobile-alt"></i> (998) 140 3087</small>
                    </div>

                    <div class="is-item">
                        <span>Morelos</span>
                        <small><i class="far fa-envelope"></i> asesoria@bufetejuridicofrias.com</small>
                        <small><i class="fas fa-mobile-alt"></i> (735) 323 2660</small>
                        <small><i class="fas fa-mobile-alt"></i> (998) 140 3087</small>
                    </div>
                </div>

            </div>
        </div>
    </footer>


    <script type="text/javascript" src="/assets/js/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="/assets/js/components.js?v=<?=$cacheVr;?>"></script>
	<script type="text/javascript" src="/assets/js/rfrias.js?v=<?=$cacheVr;?>"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-181162431-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-181162431-1');
    </script>

</body>
</html>