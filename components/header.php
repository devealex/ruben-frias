<!DOCTYPE html>
<html lang="en">
<head>

    <?php 
        $cacheVr = 4.1; 
        define('_IMG', '/assets/img/');
        $page = basename($_SERVER['SCRIPT_NAME']);
    ?>

    <meta charset="UTF-8">
    
    <?php
        include ('models/seo.php');
        $seo = new Seo_model();
        $metas = (object)$seo->metas($page);
    ?>
    <title><?= $metas->title; ?> | Bufete Jurídico Frías</title>
    <meta name="description" content="<?= $metas->description; ?>"/>


	<meta name="viewport" content="width=device-width, user-scalable=no" />
	<link rel="shortcut icon" type="image/png" href="<?=_IMG.'favicon.ico';?>"/>
	
	<!-- STYLES -->
	<link rel="stylesheet" href="/assets/css/bulma.min.css?v=<?=$cacheVr;?>">
    <link rel="stylesheet" href="/assets/css/components.min.css?v=<?=$cacheVr;?>">
    <link rel="stylesheet" href="/assets/css/rfrias.min.css?v=<?=$cacheVr;?>">

    <script src="https://kit.fontawesome.com/94233a6903.js" crossorigin="anonymous"></script>

	<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CAPTCHA -->
    <?php if ($_SERVER['PHP_SELF'] == '/index.php' || $_SERVER['PHP_SELF'] == '/contacto.php' || $_SERVER['PHP_SELF'] == '/service-page.php') { ?>
        <script src="https://www.google.com/recaptcha/api.js?render=6LcuiegUAAAAAHc8Is03ZIiLew9ZQBlILtK-NKNn"></script>
        <script type="text/javascript">
            grecaptcha.ready(function() {
                grecaptcha.execute('6LcuiegUAAAAAHc8Is03ZIiLew9ZQBlILtK-NKNn', {
                        action: 'homepage'
                    })
                    .then(function(token) {
                        document.getElementById('g-recaptcha-response').value = token;
                    });
            });
            setInterval("newCaptcha()", 60000);
            function newCaptcha(){
                grecaptcha.execute('6LcuiegUAAAAAHc8Is03ZIiLew9ZQBlILtK-NKNn', {action: 'homepage'}).then(function(token){
                    document.getElementById('g-recaptcha-response').value=token;
                });
            }
        </script>
    <?php } ?>

</head>
<body class="is-body">
    <a href="tel:(998) 440 2829" class="is-float-bubble is-whats"><i class="fab fa-whatsapp"></i></a>
    <a href="#" class="is-float-bubble is-chat"><i class="fab fa-facebook-messenger"></i></a>

    <header class="is-header">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-one-quarter is-brand">
                    <a class="logo" href="/"><img src="<?=_IMG.'bufete-juridico-frias.png';?>"></a>
                    <a class="btn-menu"><i class="fas fa-angle-double-down"></i></a>
                </div>

                <div class="column is-one-quarter is-box-contact">
                    <span><i class="fas fa-envelope-open-text"></i> Escríbenos</span>
                    <a href="mailto:asesoria@bufetejuridicofrias.com">asesoria@bufetejuridicofrias.com</a>
                </div>
    
                <div class="column is-one-quarter is-box-contact">
                    <span><i class="fas fa-phone"></i> Llámanos</span>
                    <a href="tel:9984402829">(998) 440 2829</a><a href="tel:5544155117">(998) 140 3087</a>
                </div>
    
                <div class="column is-one-quarter is-box-contact">
                    <span><i class="fab fa-whatsapp"></i> Whatsapp</span>
                    <a href="tel:7353232660">(735) 323 2660</a>
                    <a href="tel:9984402829">(998) 440 2829</a>
                </div>                    

            </div>
        </div>
    </header>

    <div class="is-subheader">
        <div class="container">
            <div class="columns">
            
                <div class="column is-full">
                    <ul class="is-menulist">
                        <li><a href="/" <?php if($page == 'index.php') { ?> class="is-current" <?php } ?>>Inicio</a></li>
                        <li><a href="/bufete-frias" <?php if($page == 'bufete-frias.php') { ?> class="is-current" <?php } ?>>Nosotros</a></li>
                        <li><a href="/servicios" <?php if($page == 'servicios.php') { ?> class="is-current" <?php } ?>>Servicios</a></li>  
                        <li><a href="/contacto" <?php if($page == 'contacto.php') { ?> class="is-current" <?php } ?>>Contacto</a></li>                  
                        <li><a href="/blog/">Blog</a></li>
                        <li class="is-social is-first-social"><a href="https://www.facebook.com/bufetejuridico.frias" target="_blank" class="is-social is-facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="is-social"><a href="https://www.instagram.com/juridicofrias/" target="_blank" class="is-social is-instagram"><i class="fab fa-instagram"></i></a></li>
                        <li class="is-lang"><a href="/en/">EN</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>