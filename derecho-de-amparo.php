<?php include('components/header.php'); ?>

<section class="is-page is-service-page-section">
    <div class="is-header-img">
        <h1>Derecho de Amparo</h1>
    </div>

    <div class="container">
        <div class="columns is-overview">
           
            <div class="column is-half">

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">Con las reformas más recientes a la Constitución y los criterios más relevantes emitidos por la Suprema Corte de Justicia, se ha producido la necesidad de que los profesionales del Derecho tengamos dominio de las normas constitucionales, de los tratados internacionales y de la jurisprudencia nacional e internacional. Así pues contamos con reconocimiento por nuestro alto grado de especialización en esas materias y en Derecho de Amparo, lo que nos afianza como la firma con mayor grado de éxito en la región. Nuestra firma ofrece distintos servicios en Derecho de Amparo, entre los que destacan:</p>

            </div>

            <div class="column is-half">

                <strong>¿Alguno de estos es tu caso y no sabes qué hacer?</strong>

                <ul class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                    <li><i class="fas fa-gavel"></i>Amparo civil</li>
                    <li><i class="fas fa-gavel"></i>Amparo penal</li>
                    <li><i class="fas fa-gavel"></i>Amparo laboral</li>
                    <li><i class="fas fa-gavel"></i>Amparo administrativo</li>
                    <li><i class="fas fa-gavel"></i>Amparo familiar</li>
                </ul>

            </div>

        </div>
    </div>

    <?php include('components/service-contact-section.php'); ?>

</section>

<?php include('components/footer.php'); ?>