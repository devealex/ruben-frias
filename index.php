<?php
include('components/header.php');

if (isset($_POST['inpName'])) {
    //Validar Captcha
    $secret_key = '6LcuiegUAAAAAIVzWH9GJa-OnsTS1ah4XN8kEBqr';
    $captcha = $_POST['g-recaptcha-response'];
    $rs = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret_key . "&response=" . $captcha);
    $rt = json_decode($rs, TRUE);
    if ($rt['success'] == true) {
        date_default_timezone_set('America/Cancun');
        //Headers
        $headers = 'MIME-Version:1.0' . "\r\n" .
            'Content-type:text/html; charset=UTF-8' . "\r\n" .
            'From: hi@bufetejuridicofrias.com' . "\r\n" .
            'Cc: rfrias@bufetejuridicofrias.com';
        //Message
        $message = '<div style="background: #F8F8F8; padding: 30px 0;">
                    <table style="width: 700px; border: 0; border-collapse: collapse; background: #fff; margin: 30px auto; border-radius: 5px;" border="0" cellpadding="20">
                        <tr>
                            <td colspan="2">
                                <a href="http://www.bufetejuridicofrias.com"><img style="width:100px; display:block; margin:0px auto 30px;" src="https://www.bufetejuridicofrias.com/assets/img/bufete-juridico-frias.png"></a>
                                <strong style="display: block; color: #73501f; font-size: 20px; text-align: center;">CONTACTO</strong>
                                <span style="display: block; font-size: 14px; text-align: center; color: #666; margin-top: 15px;">Interesado en:</span>
                                <small style="display: block; color: #73501f; font-size: 16px; text-align: center;">' . $_POST["inpService"] . '</small>
                            </td>
                        </tr>

                        <tr><td colspan="2"><strong style="font-size: 16px; color: #000;">Detalles del contacto:</strong></td></tr>

                        <tr>
                            <td><span>Nombre:</span></td>
                            <td><strong>' . $_POST['inpName'] . '</strong></td>
                        </tr>

                        <tr>
                            <td><span>Email:</span></td>
                            <td><strong>' . $_POST['inpEmail'] . '</strong></td>
                        </tr>

                        <tr>
                            <td><span>Telefono:</span></td>
                            <td><strong>' . $_POST['inpNumber'] . '</strong></td>
                        </tr>

                        <tr>
                            <td><span>Tipo de servicio:</span></td>
                            <td><strong>' . $_POST["inpService"] . '</strong></td>
                        </tr>
                    </table>

                    <p style="text-align:center;">Formulario enviado desde: ' . $_SERVER["HTTP_REFERER"] . '</p>
                    <p style="text-align:center;">Formulario enviado: ' . date('Y-m-d H:i:s') . '</p>
                </div>';
        mail('asesoria@bufetejuridicofrias.com', 'Contacto | Bufete Jurídico Frías', $message, $headers);
    }
}

?>

<section class="is-page is-home-section">
    <div class="is-slider">
        <div id="idHomeSlideShow" class="nivoSlider">
            <img src="<?= _IMG . 'bufete-frias-slider-1.jpg'; ?>">
            <img src="<?= _IMG . 'bufete-frias-slider-2.jpg'; ?>">
        </div>

        <!--<h1>Bufete Jurídico Frías</h1>-->

        <div class="is-services-grid">
            <div class="columns">
                <div class="column is-one-quarter">
                    <div class="is-item">
                        <i class="fas fa-gavel"></i>
                        <strong><a href="/derecho-laboral">Derecho Laboral</a></strong>
                        <p>Otorgamos asesoría en Demandas por DESPIDO INJUSTIFICADO, reinstalación,  renuncias, accidentes de trabajo, etc.</p>
                    </div>
                </div>

                <div class="column is-one-quarter">
                    <div class="is-item">
                        <i class="fas fa-gavel"></i>
                        <strong><a href="/derecho-penal">Derecho Penal</a></strong>
                        <p>Realizamos la mejor representación legal como asesores jurídicos en delitos culposos o dolorosos de ámbito federal o fuero común.</p>
                    </div>
                </div>

                <div class="column is-one-quarter">
                    <div class="is-item">
                        <i class="fas fa-gavel"></i>
                        <strong><a href="/derecho-mercantil">Derecho Mercantil</a></strong>
                        <p>Otorgamos asesoría de sociedades mercantiles, consultoría en contratos mercantiles, títulos de crédito, etc.</p>
                    </div>
                </div>

                <div class="column is-one-quarter">
                    <div class="is-item">
                        <i class="fas fa-gavel"></i>
                        <strong><a href="/derecho-notarial">Derecho Notarial</a></strong>
                        <p>Otorgamos la elaboración y protocolización de actas de asamblea, estatutos legales, actos registrales, certificación de documentos, etc.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--OVERVIEW-->
    <div class="container">
        <div class="columns is-overview">

            <div class="column is-half wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                <h2>Bufete Jurídico Frías</h2>

                <p>Somos una firma de Abogados con mas de 20 años de experiencia como postulantes y tenemos como misión otorgar consultorías y representación jurídica bajo el más estricto apego a las normas éticas y profesionales, actuando siempre con soluciones justas y confidenciales.</p>

                <p>Nuestro despacho está conformado por especializados Abogados con estudios de posgrado en distintas ramas jurídicas: Derecho laboral, Derecho Penal, Derecho Mercantil, Derecho Civil, Derecho de Amparo, Derecho Administrativo, Derecho Empresarial, Derecho Notarial & Derecho Migratorio.</p>

                <a href="/bufete-frias">¿Quienes Somos? <i class="fas fa-arrow-right"></i></a>
            </div>

            <div class="column is-half wow fadeInUp is-form" data-wow-duration="1s" data-wow-delay="1.5s">
                <h4>Contáctanos:</h4>
                <p>Asesoría jurídica gratuita</p>
                <form name="inpLightForm" id="inpLightForm" action="/" method="post">
                    <fieldset>
                        <label>Nombre</label>
                        <input type="text" name="inpName" id="inpName" placeholder="Nombre Completo">
                    </fieldset>
                    <fieldset>
                        <label>Email</label>
                        <input type="text" name="inpEmail" id="inpEmail" placeholder="email@dominio.com">
                    </fieldset>
                    <fieldset>
                        <label>Teléfono</label>
                        <input type="number" name="inpNumber" id="inpNumber" placeholder="##########">
                    </fieldset>
                    <fieldset>
                        <label>Estoy interesado en</label>
                        <select name="inpService" id="inpService">
                            <option value="Laboral">Derecho Laboral</option>
                            <option value="Penal">Derecho Penal</option>
                            <option value="Mercantil">Derecho Mercantil</option>
                            <option value="Civil">Derecho Civil</option>
                            <option value="Familiar">Derecho Familiar</option>
                            <option value="Notarial">Derecho Notarial</option>
                            <option value="Migratorio">Derecho Migratorio</option>
                            <option value="Empresarial">Derecho Empresarial</option>
                            <option value="Amparo">Derecho de Amparo</option>
                        </select>
                    </fieldset>
                    <fieldset>
                        <button id="btnButtonFormLight"><i class="fas fa-fax"></i> CONTACTAR</button>
                        <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response">
                    </fieldset>
                </form>
            </div>

        </div>
    </div>

    <!--COUNTER-->
    <div class="is-counters">
        <div class="container">
            <div class="columns">

                <div class="column is-one-third wow fadeIn" data-wow-duration="1s" data-wow-delay=".7s">
                    <strong>22</strong>
                    <small>AÑOS</small>
                    <span>EXPERIENCIA</span>
                </div>

                <div class="column is-one-third wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">
                    <strong>3</strong>
                    <small>ESTADOS</small>
                    <span>CON PRESENCIA</span>
                </div>

                <div class="column is-one-third wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">
                    <strong>600 <small>+</small></strong>
                    <small>CLIENTES</small>
                    <span>SATISFECHOS</span>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
    <div class="columns is-multiline is-services">

        <div class="column is-full"><h2 class="wow slideInLeft" data-wow-duration="1s" data-wow-delay=".5s">Nuestros Servicios</h2></div>

        <div class="column is-one-quarter is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay=".6s">
            <h3><a href="/derecho-laboral">Derecho Laboral</a></h3>
            <ul>
                <li><i class="fas fa-gavel"></i>Demandas por despido injustificado</li>
                <li><i class="fas fa-gavel"></i>Reinstalación</li>
                <li><i class="fas fa-gavel"></i>Renuncias</li>
                <li><i class="fas fa-gavel"></i>Accidentes de trabajo e invalidez</li>
                <li><i class="fas fa-gavel"></i>Reparto de utilidades</li>
            </ul>
        </div>

        <div class="column is-one-quarter is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay=".7s">
            <h3><a href="/derecho-penal">Derecho Penal</a></h3>
            <ul>
                <li><i class="fas fa-gavel"></i>Denuncias</li>
                <li><i class="fas fa-gavel"></i>Representación ante el mp</li>
                <li><i class="fas fa-gavel"></i>Defensa penal</li>
                <li><i class="fas fa-gavel"></i>Asesoría jurídica a victima</li>
                <li><i class="fas fa-gavel"></i>Prevención a delitos</li>
            </ul>
        </div>

        <div class="column is-one-quarter is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay=".8s">
            <h3><a href="/derecho-mercantil">Derecho Mercantil</a></h3>
            <ul>
                <li><i class="fas fa-gavel"></i>Recuperación extrajudicial</li>
                <li><i class="fas fa-gavel"></i>Pagarés</li>
                <li><i class="fas fa-gavel"></i>Representación ante condusef</li>
                <li><i class="fas fa-gavel"></i>Cheques devueltos y robados</li>
                <li><i class="fas fa-gavel"></i>Títulos de crédito</li>
            </ul>
        </div>

        <div class="column is-one-quarter is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay=".9s">
            <h3><a href="/derecho-civil">Derecho Civil</a></h3>
            <ul>
                <li><i class="fas fa-gavel"></i>Compraventa</li>
                <li><i class="fas fa-gavel"></i>Arrendamiento inmobiliario</li>
                <li><i class="fas fa-gavel"></i>Problemas de propiedad</li>
                <li><i class="fas fa-gavel"></i>Prescripción</li>
                <li><i class="fas fa-gavel"></i>Cumplimiento de contrato</li>
            </ul>
        </div>

        <div class="column is-one-quarter is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay="1s">
            <h3><a href="/derecho-familiar">Derecho Familiar</a></h3>
            <ul>
                <li><i class="fas fa-gavel"></i>Divorcios</li>
                <li><i class="fas fa-gavel"></i>Pensión alimentaria</li>
                <li><i class="fas fa-gavel"></i>Herencias y testamentos</li>
                <li><i class="fas fa-gavel"></i>Guarda y custodia</li>
                <li><i class="fas fa-gavel"></i>Reconocimiento de paternidad</li>
            </ul>
        </div>

        <div class="column is-one-quarter is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay="1.1s">
            <h3><a href="/derecho-notarial">Derecho Notarial</a></h3>
            <ul>
                <li><i class="fas fa-gavel"></i>Escrituracion</li>
                <li><i class="fas fa-gavel"></i>Testamento</li>
                <li><i class="fas fa-gavel"></i>Fe de hechos</li>
                <li><i class="fas fa-gavel"></i>Copias certificadas</li>
                <li><i class="fas fa-gavel"></i>Poderes notariales</li>
            </ul>
        </div>

        <div class="column is-one-quarter is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay="1.1s">
            <h3><a href="/derecho-migratorio">Derecho Migratorio</a></h3>
            <ul>
                <li><i class="fas fa-gavel"></i>Naturalizaciones</li>
                <li><i class="fas fa-gavel"></i>Residencias temporales</li>
                <li><i class="fas fa-gavel"></i>Visas de trabajo</li>
                <li><i class="fas fa-gavel"></i>Residencias permanentes</li>
                <li><i class="fas fa-gavel"></i>Regularización de estatus migratorio</li>
            </ul>
        </div>

        <div class="column is-one-quarter is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay="1.1s">
            <h3><a href="/derecho-empresarial">Derecho Empresarial</a></h3>
            <ul>
                <li><i class="fas fa-gavel"></i>Startups</li>
                <li><i class="fas fa-gavel"></i>Auditoria legal</li>
                <li><i class="fas fa-gavel"></i>Cumplimiento normativo</li>
                <li><i class="fas fa-gavel"></i>Administración jurídica</li>
                <li><i class="fas fa-gavel"></i>Mediación</li>
            </ul>
        </div>

        <div class="column is-half is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay="1.1s">
            <h3><a href="/derecho-de-amparo">Derecho de Amparo</a></h3>
            <ul>
                <li><i class="fas fa-gavel"></i>Amparo civil</li>
                <li><i class="fas fa-gavel"></i>Amparo penal</li>
                <li><i class="fas fa-gavel"></i>Amparo laboral</li>
                <li><i class="fas fa-gavel"></i>Amparo administrativo</li>
                <li><i class="fas fa-gavel"></i>Amparo familiar</li>
            </ul>
        </div>

                

    </div>

    <div class="columns is-multiline is-locations">
        <div class="column is-full"><h2 class="wow slideInRight" data-wow-duration="1s" data-wow-delay=".5s">Estamos en...</h2></div>

        <div class="column is-one-third is-item wow slideInRight" data-wow-duration="1s" data-wow-delay=".7s">
            <img src="<?= _IMG . 'cancun-bufete-frias.jpg'; ?>">
            <strong>Cancún / Playa del Carmen</strong>
            <span><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
        </div>

        <div class="column is-one-third is-item wow slideInRight" data-wow-duration="1s" data-wow-delay=".6s">
            <img src="<?= _IMG . 'mexico-bufete-frias.jpg'; ?>">
            <strong>Ciudad de México</strong>
            <span><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
        </div>

        <div class="column is-one-third is-item wow slideInRight" data-wow-duration="1s" data-wow-delay=".8s">
            <img src="<?= _IMG . 'puebla-bufete-frias.jpg'; ?>">
            <strong>Morelos</strong>
            <span><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>
            <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
        </div>

        </div>

    </div>
    </div>

</section>

<?php include('components/footer.php'); ?>