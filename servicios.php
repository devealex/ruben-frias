<?php include('components/header.php'); ?>

<section class="is-page is-service-section">
    <div class="is-header-img">
        <h1>Bufete Jurídico Frías</h1>
    </div>

    <div class="container">
        <div class="columns">
            <div class="column is-full is-overview">
                <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">Nuestros Servicios</h2>

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".9s">Contamos con un equipo de profesionales, especialistas en diferentes ramas del derecho al servicio de la comunidad empresarial en México y de una diversidad de clientes e inversionistas de los principales centros de negocios del País. Asimismo, con el fin de atender y acompañar a todos nuestros clientes dentro de un ámbito de honestidad, guardamos una estrecha relación con una red formada por las mejores firmas legales en toda la República Mexicana.</p>

            </div>
        </div>

        <div class="columns is-multiline is-variable is-5 is-services">
            
            <div class="column is-one-quarter is-item is-one-quarter wow zoomIn" data-wow-duration="1s" data-wow-delay=".5s">
                <a href="/derecho-laboral"><img src="<?=_IMG.'derecho-laboral.jpg';?>"></a>
                <h3><a href="/derecho-laboral">Derecho Laboral</a></h3>
                <p>Otorgamos asesoría en Demandas por DESPIDO INJUSTIFICADO, reinstalación,  renuncias, accidentes de trabajo, etc.</p>
                <a href="/derecho-laboral">Saber más <i class="fas fa-arrow-right"></i></a>
            </div>

            <div class="column is-one-quarter is-item is-one-quarter wow zoomIn" data-wow-duration="1s" data-wow-delay=".7s">
                <a href="/derecho-penal"><img src="<?=_IMG.'derecho-penal.jpg';?>"></a>
                <h3><a href="/derecho-penal">Derecho Penal</a></h3>
                <p>Como Abogados Penalistas, somos un equipo altamente calificado y  especializado en Derecho Penal, brindando atención oportuna y manejo de conflicto en tiempo real ante cualquier tribunal.</p>
                <a href="/derecho-penal">Saber más <i class="fas fa-arrow-right"></i></a>
            </div>

            <div class="column is-one-quarter is-item is-one-quarter wow zoomIn" data-wow-duration="1s" data-wow-delay=".9s">
                <a href="/derecho-mercantil"><img src="<?=_IMG.'derecho-mercantil.jpg';?>"></a>
                <h3><a href="/derecho-mercantil">Derecho Mercantil</a></h3>
                <p>Otorgamos asesoría de sociedades mercantiles, consultoría en contratos mercantiles, títulos de crédito, etc.</p>
                <a href="/derecho-mercantil">Saber más <i class="fas fa-arrow-right"></i></a>
            </div>

            <div class="column is-one-quarter is-item is-one-quarter wow zoomIn" data-wow-duration="1s" data-wow-delay="1.1s">
                <a href="/derecho-civil"><img src="<?=_IMG.'derecho-civil.jpg';?>"></a>
                <h3><a href="/derecho-civil">Derecho Civil</a></h3>
                <p>Asesoramos de modo inmediato las relaciones personales o patrimoniales entre personas físicas o morales.</p>
                <a href="/derecho-civil">Saber más <i class="fas fa-arrow-right"></i></a>
            </div>

            <div class="column is-one-quarter is-item is-one-quarter wow zoomIn" data-wow-duration="1s" data-wow-delay="1.1s">
                <a href="/derecho-familiar"><img src="<?=_IMG.'derecho-familiar.jpg';?>"></a>
                <h3><a href="/derecho-familiar">Derecho Familiar</a></h3>
                <p>Esta firma legal le implementa y desarrolla estrategias familiares que permitan prevenir y resolver los conflictos legales</p>
                <a href="/derecho-familiar">Saber más <i class="fas fa-arrow-right"></i></a>
            </div>

            <div class="column is-one-quarter is-item is-one-quarter wow zoomIn" data-wow-duration="1s" data-wow-delay="1.1s">
                <a href="/derecho-notarial"><img src="<?=_IMG.'derecho-notarial.jpg';?>"></a>
                <h3><a href="/derecho-notarial">Derecho Nótarial</a></h3>
                <p>Otorgamos la elaboración y protocolización de actas de asamblea, estatutos legales, actos registrales, certificación de documentos, etc.</p>
                <a href="/derecho-notarial">Saber más <i class="fas fa-arrow-right"></i></a>
            </div>

            <div class="column is-one-quarter is-item is-one-quarter wow zoomIn" data-wow-duration="1s" data-wow-delay="1.1s">
                <a href="/derecho-migratorio"><img src="<?=_IMG.'derecho-migratorio.jpg';?>"></a>
                <h3><a href="/derecho-migratorio">Derecho Migratorio</a></h3>
                <p>Nos apasiona brindar servicios migratorios y legales de calidad, representando tanto a empresas grandes o pequeñas.</p>
                <a href="/derecho-migratorio">Saber más <i class="fas fa-arrow-right"></i></a>
            </div>

            <div class="column is-one-quarter is-item is-one-quarter wow zoomIn" data-wow-duration="1s" data-wow-delay="1.1s">
                <a href="/derecho-empresarial"><img src="<?=_IMG.'derecho-empresarial.jpg';?>"></a>
                <h3><a href="/derecho-empresarial">Derecho Empresarial</a></h3>
                <p>Asesoramos constantemente a empresas desde su constitución mediante la creación de estrategias legales.</p>
                <a href="/derecho-empresarial">Saber más <i class="fas fa-arrow-right"></i></a>
            </div>

            <div class="column is-one-quarter is-item is-one-quarter wow zoomIn" data-wow-duration="1s" data-wow-delay="1.1s">
                <a href="/derecho-de-amparo"><img src="<?=_IMG.'derecho-amparo.jpg';?>"></a>
                <h3><a href="/derecho-de-amparo">Derecho de Amparo</a></h3>
                <p>Contamos con reconocimiento por nuestro alto grado de especialización en Derecho de Amparo.</p>
                <a href="/derecho-de-amparo">Saber más <i class="fas fa-arrow-right"></i></a>
            </div>

        </div>
    </div>


    <div class="is-locations">
        <div class="container">
            <div class="columns">
            
                <div class="column is-one-third wow slideInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                    <h3>¿Dónde estamos?</h3>
                    <p>Nos encontramos en Morelos, Quintana Roo y Ciudad de México. También contamos con una red formada por las mejores firmas legales en toda la República Mexicana.</p>
                </div>

                <div class="column is-two-thirds">
                    <div class="columns">

                        <div class="column is-one-third is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay=".9s">
                            <img src="<?=_IMG.'cancun-bufete-frias.jpg';?>">   
                            <strong>Cancún / Playa del Carmen</strong> 
                            <span><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
                            <span><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>
                            <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
                        </div>

                        <div class="column is-one-third is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay=".7s">
                            <img src="<?=_IMG.'mexico-bufete-frias.jpg';?>">    
                            <strong>Ciudad de México</strong>
                            <span><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
                            <span><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>
                            <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
                        </div>

                        <div class="column is-one-third is-item wow slideInLeft" data-wow-duration="1s" data-wow-delay="1.1s">
                            <img src="<?=_IMG.'puebla-bufete-frias.jpg';?>">  
                            <strong>Morelos</strong>  
                            <span><i class="far fa-envelope"></i> rfrias@bufetejuridicofrias.com</span>
                            <span><i class="fas fa-mobile-alt"></i> (998) 440 2829</span>
                            <span><i class="fas fa-mobile-alt"></i> (998) 140 3087</span>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>


</section>

<?php include('components/footer.php'); ?>