<?php include('components/header.php'); ?>

<section class="is-page is-service-page-section">
    <div class="is-header-img">
        <h1>Derecho Civil</h1>
    </div>

    <div class="container">
        <div class="columns is-overview">
           
            <div class="column is-half">

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">El Derecho Civil es el conjunto de normas jurídicas que regulan las relaciones personales o patrimoniales entre personas físicas o morales, de carácter privado y público, encontrándose presente en todas las relaciones y negocios realizados entre particulares. Por lo cual, lo asesoramos de modo inmediato en todas las dudas que pudieran surgir en su vida diaria, bien a través de las consultas personales o de informes jurídicos por escrito, defendiendo sus intereses a nivel judicial si fuera necesario. Nuestra firma ofrece distintos servicios en materia Civil, entre los que destacan:</p>

            </div>

            <div class="column is-half">

                <strong>¿Alguno de estos es tu caso y no sabes qué hacer?</strong>

                <ul class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                    <li><i class="fas fa-gavel"></i>Compraventa</li>
                    <li><i class="fas fa-gavel"></i>Arrendamiento inmobiliario</li>
                    <li><i class="fas fa-gavel"></i>Problemas de propiedad</li>
                    <li><i class="fas fa-gavel"></i>Prescripción</li>
                    <li><i class="fas fa-gavel"></i>Cumplimiento de contrato</li>
                    <li><i class="fas fa-gavel"></i>Incumplimiento de contrato</li>
                    <li><i class="fas fa-gavel"></i>Medios preparatorios</li>
                </ul>

            </div>

        </div>
    </div>

    <?php include('components/service-contact-section.php'); ?>

</section>

<?php include('components/footer.php'); ?>