<?php include('components/header.php'); ?>

<section class="is-page is-service-page-section">
    <div class="is-header-img">
        <h1>Derecho Laboral</h1>
    </div>

    <div class="container">
        <div class="columns is-overview">
           
            <div class="column is-half">

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">Mediante nuestros servicios, esta firma legal le implementa y desarrolla estrategias familiares que permitan prevenir y resolver los conflictos legales que se lleguen a dar en sus modalidades de estado civil, derecho hereditario, así como transmisión, modificación o extinción de bienes, derechos u obligaciones entre miembros de una familia. Nuestra firma ofrece distintos servicios en materia Familiar, entre los que destacan:</p>

            </div>

            <div class="column is-half">

                <strong>¿Alguno de estos es tu caso y no sabes qué hacer?</strong>

                <ul class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                    <li><i class="fas fa-gavel"></i>Divorcios</li>
                    <li><i class="fas fa-gavel"></i>Pensión alimentaria</li>
                    <li><i class="fas fa-gavel"></i>Herencias y testamentos</li>
                    <li><i class="fas fa-gavel"></i>Guarda y custodia</li>
                    <li><i class="fas fa-gavel"></i>Reconocimiento de paternidad</li>
                    <li><i class="fas fa-gavel"></i>Patria potestad</li>
                </ul>

            </div>

        </div>
    </div>

    <?php include('components/service-contact-section.php'); ?>

</section>

<?php include('components/footer.php'); ?>