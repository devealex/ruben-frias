<?php include('components/header.php'); ?>

<section class="is-page is-service-page-section">
    <div class="is-header-img">
        <h1>Derecho Migratorio</h1>
    </div>

    <div class="container">
        <div class="columns is-overview">
           
            <div class="column is-half">

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">  Evolucionando nos hemos convertido en un despacho legal sofisticado. Nos apasiona brindar servicios migratorios y legales de calidad, representando tanto a empresas grandes o pequeñas, así como a individuos que atraviesan el procedimiento migratorio mexicano en cada una de sus etapas. Nuestra meta es hacer sencillo cualquier tramite migratorio. Nuestra firma ofrece distintos servicios en materia Migratoria, entre los que destacan:</p>

            </div>

            <div class="column is-half">

                <strong>¿Alguno de estos es tu caso y no sabes qué hacer?</strong>

                <ul class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                    <li><i class="fas fa-gavel"></i>Naturalizaciones</li>
                    <li><i class="fas fa-gavel"></i>Residencias temporales</li>
                    <li><i class="fas fa-gavel"></i>Visas de trabajo</li>
                    <li><i class="fas fa-gavel"></i>Residencias permanentes</li>
                    <li><i class="fas fa-gavel"></i>Regularización de estatus migratorio</li>
                    <li><i class="fas fa-gavel"></i>Certificación y apostillados de documentos</li>
                </ul>

            </div>

        </div>
    </div>

    <?php include('components/service-contact-section.php'); ?>

</section>

<?php include('components/footer.php'); ?>