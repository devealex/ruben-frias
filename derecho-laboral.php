<?php include('components/header.php'); ?>

<section class="is-page is-service-page-section">
    <div class="is-header-img">
        <h1>Derecho Laboral</h1>
    </div>

    <div class="container">
        <div class="columns is-overview">
           
            <div class="column is-half">

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s"> Como abogados laboralistas, somos profesionales formados con un alto grado de especialización en la rama del Derecho Laboral. Llevamos estrecha relación con sindicatos y elaboramos contratos especializados en la materia para evitar conflictos futuros.</p>

                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".9s">Ofrecemos una vasta experiencia en asesoramiento y defensa legal, respecto de todas las materias integradas en el ámbito del derecho del trabajo, tanto a nivel de particulares como de empresas. Nuestra firma ofrece distintos servicios en materia Laboral, entre los que destacan:</p>

            </div>

            <div class="column is-half">

                <strong>¿Alguno de estos es tu caso y no sabes qué hacer?</strong>

                <ul class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                    <li><i class="fas fa-gavel"></i>Demandas por despido injustificado</li>
                    <li><i class="fas fa-gavel"></i>Reinstalación</li>
                    <li><i class="fas fa-gavel"></i>Renuncias</li>
                    <li><i class="fas fa-gavel"></i>Accidentes de trabajo e invalidez</li>
                    <li><i class="fas fa-gavel"></i>Reparto de utilidades</li>
                    <li><i class="fas fa-gavel"></i>Pensión por cesantía en edad avanzada</li>
                    <li><i class="fas fa-gavel"></i>Recuperación de aportaciones al sistema de ahorro para el retiro</li>
                    <li><i class="fas fa-gavel"></i>Fallecimiento, pensión de viudez, orfandad, etc.</li>
                    <li><i class="fas fa-gavel"></i>Liquidaciones</li>
                    <li><i class="fas fa-gavel"></i>Elaboración de contratos laborales</li>
                </ul>

            </div>

        </div>
    </div>

    <?php include('components/service-contact-section.php'); ?>

</section>

<?php include('components/footer.php'); ?>